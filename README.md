# html_preview

A React app that gives you an instant preview of any HTML code.

## Building/running
Run
```shell
$ npm install
```
and then
```shell
$ npm start
```
to open it up in a web server, or
```shell
$ npm run build
```
to build the files (found in build/)

## Examples:

* [Base page](https://static.henderea.com/html_preview/)
* [Sample 1](https://static.henderea.com/html_preview/?content=DwEwlgbgBAzgLgTwDYFMC8AiOKAecC0AhkmAOYB2AXFAMYrnYBOA3BgHwBQoksiqmAB0Ihw5UpQBMABgE5mAW0KNSYKjLkAjAPaMQKRtQCM6qFoCucGCjjNwMAUkIJKqkuRT4NSLTQDWzLQh9ADNvAHdKAAswEXpWTm5oeGR0DCERVXF1BSUVNVkc5VVPLTg4LXkjKWztXX0q2ShVKxs7BycXcjcPLx9%2FMJi4SMoAVkMJAsiUMki4UfGCwJDwqJi9cniucCS%2BVMUi%2FLl00SyCtsdnV1Ue7z9mAZAh%2BYm5KZm5sZeAoMZQrQjorENuxgGB5KRYIwaJgAPRgwikFAwOE0LTkACCwWCqjAiAAdAIxBgoA8hphPsS3qRZuTxsTkvw0sITpRsvs8qzWFAYWxgDDtgl%2BZBBQKuEKIGwgA%3D)